Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xmahjongg
Upstream-Contact: Eddie Kohler <eddietwo@lcs.mit.edu>
Source: http://www.lcdf.org/xmahjongg/

Files: *
Copyright: 1993-2000 Eddie Kohler
License: GPL-2+

Files: liblcdf/clp.c include/lcdf/clp.h
Copyright: Copyright (c) 1997-2005 Eddie Kohler, kohler@cs.ucla.edu
License: MIT

Files: liblcdf/giffunc.c liblcdf/gifread.c liblcdf/gifx.c
       include/lcdfgif/gif.h include/lcdfgif/gifx.h
Copyright: Copyright (C) 1997-9 Eddie Kohler, eddietwo@lcs.mit.edu
License: GIF-GPL-2+
  This file is part of the GIF library.
  .
  The GIF library is free software*. It is distributed under the GNU General
  Public License, version 2 or later; you can copy, distribute, or alter it
  at will, as long as this notice is kept intact and this source code is made
  available. There is no warranty, express or implied.
  .
  *There is a patent on the LZW compression method used by GIFs, and included
  in gifwrite.c. Unisys, the patent holder, allows the compression algorithm
  to be used without a license in software distributed at no cost to the
  user. The decompression algorithm is not patented.
  .
  On Debian systems, the complete text of the GNU General Public License
  can be found in file "/usr/share/common-licenses/GPL-2".

Files: liblcdf/permstr.cc include/lcdf/permstr.hh
Copyright: Copyright (c) 1998-2004 Eddie Kohler
License: GPL-2+

Files: liblcdf/straccum.cc liblcdf/string.cc
       include/lcdf/straccum.hh include/lcdf/string.hh
Copyright: Copyright (c) 1999-2000 Massachusetts Institute of Technology
           Copyright (c) 2001-2005 Eddie Kohler
License: MIT

Files: liblcdf/vectorv.cc
Copyright: Copyright (c) 1999-2004 Massachusetts Institute of Technology
License: MIT

Files: include/lcdf/vector.cc include/lcdf/vector.hh
Copyright: Copyright (c) 1999-2000 Massachusetts Institute of Technology
           Copyright (c) 2001-2003 International Computer Science Institute
License: MIT

Files: share/tiles/dorothys.gif
Copyright: Copyright (c) 2000  Dorothy Robinson <mokuren@teleport.com>
License: GPL-2+

Files: share/tiles/dorwhite.gif
Copyright: Copyright (c) 1988  Dorothy Robinson <mokuren@teleport.com>
License: GPL-2+

Files: debian/*
Copyright: 1998-2005,  Dave Swegen <dsw@debian.org>
           2009, 2010, Peter Pentchev <roam@ringlet.net>
           2016,       Markus Koschany <apo@debian.org>
License: GPL-2+

License: GPL-2+
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  .
  On Debian systems, the complete text of the GNU General Public License
  can be found in file "/usr/share/common-licenses/GPL-2".

License: MIT
  Permission is hereby granted, free of charge, to any person obtaining a
  copy of this software and associated documentation files (the "Software"),
  to deal in the Software without restriction, subject to the conditions
  listed in the Click LICENSE file. These conditions include: you must
  preserve this copyright notice, and you cannot mention the copyright
  holders in advertising related to the Software without their permission.
  The Software is provided WITHOUT ANY WARRANTY, EXPRESS OR IMPLIED. This
  notice is a summary of the Click LICENSE file; the license in that file is
  legally binding.
